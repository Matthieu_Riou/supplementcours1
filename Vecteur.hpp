#ifndef VECTEUR_HPP
#define VECTEUR_HPP

#include <sstream> //Pour utiliser std::ostream dans operator<<
#include "Point.hpp"

class Vecteur
{
    private:
        Point origine;
        Point arrivee;
    
    public:
        Vecteur(Point o, Point a);
        
        Point getOrigine() const;
        Point getArrivee() const;
        /* J'ai décidé de ne pas mettre de setter. 
           Un vecteur, une fois créé, n'est pas modifiable.
           Il faudra en créer un nouveau.
        */
        
        Point appliquerSur(Point p) const;
		Vecteur& operator+=(Vecteur const& v2);
};

Vecteur operator+(Vecteur const& v1, Vecteur const& v2); 
std::ostream& operator<<(std::ostream& os, Vecteur const& v);

#endif