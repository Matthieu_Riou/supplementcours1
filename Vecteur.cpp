#include "Vecteur.hpp"

Vecteur::Vecteur(Point o, Point a) : origine(o), arrivee(a)
{}

Point Vecteur::getOrigine() const
{
    return this->origine;
}

Point Vecteur::getArrivee() const
{
    return this->arrivee;
}

Point Vecteur::appliquerSur(Point p) const
{
    // On calcule la différence sur les x et sur les y entre arrivee et origine
    int diff_x = this->arrivee.x - this->origine.x;
    int diff_y = this->arrivee.y - this->origine.y;
    
    // On applique cette même différence en prenant p comme point d'origine
    Point resultat;
    resultat.x = p.x + diff_x;
    resultat.y = p.y + diff_y;
    
    return resultat;
}

Vecteur& Vecteur::operator+=(Vecteur const& v2)
{
    // Cette fois on additionne this et v2
    
    // this->origine ne change pas, puisqu'il garde son point d'origine
	
	// this->arrivee prend la valeur du point d'arrivée de this + v2
    this->arrivee = v2.appliquerSur(this->arrivee);
	    
    return *this;
}

// operator+ n'appartient pas à classe Vecteur, donc il n'y a pas besoin d'écrire Vector::operator+
Vecteur operator+(Vecteur const& v1, Vecteur const& v2)
{
    // On a exactement le même code que la fonction add 
    
    Point add_origine = v1.getOrigine();
    Point add_arrivee = v2.appliquerSur(v1.getArrivee());
    
    return Vecteur(add_origine, add_arrivee);
}

std::ostream& operator<<(std::ostream& os, Vecteur const& v)
{
    /* On va afficher un vector sous la notation : 
      "(x_origin, y_origin) -> (x_arrivee, y_arrivee)"
    */
    
    // Affichage du point d'origine
    os << "(" << v.getOrigine().x << ", " << v.getOrigine().y << ")"; 
    os << "->"; 
    //Affichage du point d'arrivee
    os << "(" << v.getArrivee().x << ", " << v.getArrivee().y << ")"; 
    return os;
}