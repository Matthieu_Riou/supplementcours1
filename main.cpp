#include <iostream>

#include "Vecteur.hpp"
#include "Point.hpp"

using namespace std;

int main()
{
	// Un premier vecteur
	Point origine;
	origine.x = 5;
	origine.y = 5;

	Point arrivee;
	arrivee.x = 10;
	arrivee.y = 15;

	Vecteur v1(origine, arrivee);


	// Un deuxieme vecteur
	Point origine2;
	origine2.x = 0;
	origine2.y = 0;

	Point arrivee2;
	arrivee2.x = -5;
	arrivee2.y = 10;

	Vecteur v2(origine2, arrivee2);
	
	cout << "Vecteur 1 : " << v1 << endl;
	cout << "Vecteur 2 : " << v2 << endl;
	
	cout << endl << endl;
	cout << "v3 = v1 + v2" << endl;
	Vecteur v3 = v1 + v2;
	cout << "Vecteur 1 : " << v1 << endl;
	cout << "Vecteur 2 : " << v2 << endl;
	cout << "Vecteur 3 : " << v3 << endl;
	
	cout << endl << endl;
	cout << "v1 += v2" << endl;
	v1 += v2;
	cout << "Vecteur 1 : " << v1 << endl;
	cout << "Vecteur 2 : " << v2 << endl;
	cout << "Vecteur 3 : " << v3 << endl;
	
	
	return 0;
}